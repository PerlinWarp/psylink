EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:Earth #PWR026
U 1 1 608629D5
P 3550 2100
F 0 "#PWR026" H 3550 1850 50  0001 C CNN
F 1 "Earth" H 3550 1950 50  0001 C CNN
F 2 "" H 3550 2100 50  0001 C CNN
F 3 "~" H 3550 2100 50  0001 C CNN
	1    3550 2100
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR031
U 1 1 608E21E1
P 4450 2100
F 0 "#PWR031" H 4450 1850 50  0001 C CNN
F 1 "Earth" H 4450 1950 50  0001 C CNN
F 2 "" H 4450 2100 50  0001 C CNN
F 3 "~" H 4450 2100 50  0001 C CNN
	1    4450 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 1400 3550 1500
$Comp
L power:GNDS #PWR030
U 1 1 608F7B0A
P 4150 1800
F 0 "#PWR030" H 4150 1550 50  0001 C CNN
F 1 "GNDS" V 4200 1600 50  0000 C CNN
F 2 "" H 4150 1800 50  0001 C CNN
F 3 "" H 4150 1800 50  0001 C CNN
	1    4150 1800
	0    -1   -1   0   
$EndComp
Text Notes 4650 5400 2    50   ~ 0
https://psylink.me/c10
$Comp
L power:Earth #PWR022
U 1 1 60864F96
P 3150 2100
F 0 "#PWR022" H 3150 1850 50  0001 C CNN
F 1 "Earth" H 3150 1950 50  0001 C CNN
F 2 "" H 3150 2100 50  0001 C CNN
F 3 "~" H 3150 2100 50  0001 C CNN
	1    3150 2100
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR021
U 1 1 614581CF
P 3150 1400
F 0 "#PWR021" H 3150 1250 50  0001 C CNN
F 1 "VCC" H 3167 1573 50  0000 C CNN
F 2 "" H 3150 1400 50  0001 C CNN
F 3 "" H 3150 1400 50  0001 C CNN
	1    3150 1400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R14
U 1 1 608E42F4
P 3150 1850
F 0 "R14" V 3150 1850 50  0000 C CNN
F 1 "100K/1%" V 3250 1900 50  0000 C TNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3080 1850 50  0001 C CNN
F 3 "~" H 3150 1850 50  0001 C CNN
	1    3150 1850
	-1   0    0    1   
$EndComp
$Comp
L Device:R R13
U 1 1 6084FC80
P 3150 1550
F 0 "R13" V 3150 1550 50  0000 C CNN
F 1 "100K/1%" V 3250 1500 50  0000 C TNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3080 1550 50  0001 C CNN
F 3 "~" H 3150 1550 50  0001 C CNN
	1    3150 1550
	-1   0    0    1   
$EndComp
$Comp
L power:VCC #PWR025
U 1 1 6145A11F
P 3550 1400
F 0 "#PWR025" H 3550 1250 50  0001 C CNN
F 1 "VCC" H 3567 1573 50  0000 C CNN
F 2 "" H 3550 1400 50  0001 C CNN
F 3 "" H 3550 1400 50  0001 C CNN
	1    3550 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 1700 3150 1700
Connection ~ 3150 1700
Wire Wire Line
	3150 2000 3150 2100
$Comp
L Device:C C14
U 1 1 608DD1DD
P 4300 1500
F 0 "C14" V 4045 1500 50  0000 C CNN
F 1 "100uF" V 4137 1500 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4338 1350 50  0001 C CNN
F 3 "~" H 4300 1500 50  0001 C CNN
	1    4300 1500
	0    1    1    0   
$EndComp
Wire Wire Line
	1500 1850 1750 1850
Connection ~ 1500 1850
Connection ~ 1500 1650
Wire Wire Line
	1450 1850 1500 1850
$Comp
L Device:R_Small R2
U 1 1 6095A52B
P 1500 1750
F 0 "R2" V 1500 1750 39  0000 C CNN
F 1 "1M/1%" H 1600 1600 50  0000 C TNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1430 1750 50  0001 C CNN
F 3 "~" H 1500 1750 50  0001 C CNN
	1    1500 1750
	1    0    0    -1  
$EndComp
Text Label 1100 1850 0    50   ~ 0
in2
Text Label 2500 1650 0    50   ~ 0
out1
Wire Wire Line
	2600 1650 2450 1650
Wire Wire Line
	1250 1850 1100 1850
Wire Wire Line
	1250 1450 1100 1450
Text Label 1100 1450 0    50   ~ 0
in1
Entry Wire Line
	1000 1950 1100 1850
Entry Wire Line
	1000 1550 1100 1450
$Comp
L power:GNDS #PWR013
U 1 1 608F17B2
P 2150 1950
F 0 "#PWR013" H 2150 1700 50  0001 C CNN
F 1 "GNDS" V 2100 1750 50  0000 C CNN
F 2 "" H 2150 1950 50  0001 C CNN
F 3 "" H 2150 1950 50  0001 C CNN
	1    2150 1950
	0    -1   -1   0   
$EndComp
Connection ~ 2050 1350
Wire Wire Line
	2150 1350 2050 1350
Wire Wire Line
	1500 1450 1750 1450
Wire Wire Line
	1400 1650 1500 1650
Connection ~ 1500 1450
Wire Wire Line
	2350 1350 2400 1350
$Comp
L Device:C_Small C9
U 1 1 608CA25E
P 2250 1350
F 0 "C9" V 2350 1350 50  0000 C CNN
F 1 "100nF" V 2100 1350 50  0000 C TNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2288 1200 50  0001 C CNN
F 3 "~" H 2250 1350 50  0001 C CNN
	1    2250 1350
	0    -1   -1   0   
$EndComp
$Comp
L power:Earth #PWR017
U 1 1 608D13B5
P 2400 1350
F 0 "#PWR017" H 2400 1100 50  0001 C CNN
F 1 "Earth" H 2400 1200 50  0001 C CNN
F 2 "" H 2400 1350 50  0001 C CNN
F 3 "~" H 2400 1350 50  0001 C CNN
	1    2400 1350
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Instrumentation:INA128 U1
U 1 1 6089F0CF
P 2050 1650
F 0 "U1" H 2300 1550 50  0000 L CNN
F 1 "INA128" H 2350 1500 50  0000 C TNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 2150 1650 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/ina128.pdf" H 2150 1650 50  0001 C CNN
	1    2050 1650
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR06
U 1 1 608D7F4C
P 2050 1950
F 0 "#PWR06" H 2050 1700 50  0001 C CNN
F 1 "Earth" H 2050 1800 50  0001 C CNN
F 2 "" H 2050 1950 50  0001 C CNN
F 3 "~" H 2050 1950 50  0001 C CNN
	1    2050 1950
	-1   0    0    -1  
$EndComp
$Comp
L power:VCC #PWR05
U 1 1 614556C0
P 2050 1350
F 0 "#PWR05" H 2050 1200 50  0001 C CNN
F 1 "VCC" H 2050 1500 50  0000 C CNN
F 2 "" H 2050 1350 50  0001 C CNN
F 3 "" H 2050 1350 50  0001 C CNN
	1    2050 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 1450 1500 1450
$Comp
L Device:R_Small R9
U 1 1 608BC652
P 1700 1650
F 0 "R9" V 1700 1650 39  0000 C CNN
F 1 "1K/1%" V 1600 1650 50  0000 C TNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1630 1650 50  0001 C CNN
F 3 "~" H 1700 1650 50  0001 C CNN
	1    1700 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 1750 1750 1750
Wire Wire Line
	1700 1550 1750 1550
$Comp
L power:GNDS #PWR01
U 1 1 608F6785
P 1400 1650
F 0 "#PWR01" H 1400 1400 50  0001 C CNN
F 1 "GNDS" H 1400 1500 50  0000 C CNN
F 2 "" H 1400 1650 50  0001 C CNN
F 3 "" H 1400 1650 50  0001 C CNN
	1    1400 1650
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C1
U 1 1 6093EE1D
P 1350 1450
F 0 "C1" V 1450 1450 50  0000 C CNN
F 1 "100pF" V 1500 1450 50  0000 C TNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1388 1300 50  0001 C CNN
F 3 "~" H 1350 1450 50  0001 C CNN
	1    1350 1450
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R1
U 1 1 60941D1F
P 1500 1550
F 0 "R1" V 1500 1550 39  0000 C CNN
F 1 "1M/1%" H 1600 1750 50  0000 C TNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1430 1550 50  0001 C CNN
F 3 "~" H 1500 1550 50  0001 C CNN
	1    1500 1550
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C2
U 1 1 6095A525
P 1350 1850
F 0 "C2" V 1450 1850 50  0000 C CNN
F 1 "100pF" V 1500 1850 50  0000 C TNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1388 1700 50  0001 C CNN
F 3 "~" H 1350 1850 50  0001 C CNN
	1    1350 1850
	0    1    1    0   
$EndComp
Wire Wire Line
	1500 2900 1750 2900
Connection ~ 1500 2900
Connection ~ 1500 2700
Wire Wire Line
	1450 2900 1500 2900
$Comp
L Device:R_Small R4
U 1 1 614840A0
P 1500 2800
F 0 "R4" V 1500 2800 39  0000 C CNN
F 1 "1M/1%" H 1600 2650 50  0000 C TNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1430 2800 50  0001 C CNN
F 3 "~" H 1500 2800 50  0001 C CNN
	1    1500 2800
	1    0    0    -1  
$EndComp
Text Label 2500 2700 0    50   ~ 0
out2
Wire Wire Line
	2600 2700 2450 2700
Wire Wire Line
	1250 2900 1100 2900
Wire Wire Line
	1250 2500 1100 2500
Entry Wire Line
	1000 3000 1100 2900
Entry Wire Line
	1000 2600 1100 2500
$Comp
L power:GNDS #PWR014
U 1 1 614840AF
P 2150 3000
F 0 "#PWR014" H 2150 2750 50  0001 C CNN
F 1 "GNDS" V 2100 2800 50  0000 C CNN
F 2 "" H 2150 3000 50  0001 C CNN
F 3 "" H 2150 3000 50  0001 C CNN
	1    2150 3000
	0    -1   -1   0   
$EndComp
Connection ~ 2050 2400
Wire Wire Line
	2150 2400 2050 2400
Wire Wire Line
	1500 2500 1750 2500
Wire Wire Line
	1400 2700 1500 2700
Connection ~ 1500 2500
Wire Wire Line
	2350 2400 2400 2400
$Comp
L Device:C_Small C10
U 1 1 614840BB
P 2250 2400
F 0 "C10" V 2350 2400 50  0000 C CNN
F 1 "100nF" V 2100 2400 50  0000 C TNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2288 2250 50  0001 C CNN
F 3 "~" H 2250 2400 50  0001 C CNN
	1    2250 2400
	0    -1   -1   0   
$EndComp
$Comp
L power:Earth #PWR018
U 1 1 614840C1
P 2400 2400
F 0 "#PWR018" H 2400 2150 50  0001 C CNN
F 1 "Earth" H 2400 2250 50  0001 C CNN
F 2 "" H 2400 2400 50  0001 C CNN
F 3 "~" H 2400 2400 50  0001 C CNN
	1    2400 2400
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Instrumentation:INA128 U2
U 1 1 614840C7
P 2050 2700
F 0 "U2" H 2300 2600 50  0000 L CNN
F 1 "INA128" H 2350 2550 50  0000 C TNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 2150 2700 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/ina128.pdf" H 2150 2700 50  0001 C CNN
	1    2050 2700
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR08
U 1 1 614840CD
P 2050 3000
F 0 "#PWR08" H 2050 2750 50  0001 C CNN
F 1 "Earth" H 2050 2850 50  0001 C CNN
F 2 "" H 2050 3000 50  0001 C CNN
F 3 "~" H 2050 3000 50  0001 C CNN
	1    2050 3000
	-1   0    0    -1  
$EndComp
$Comp
L power:VCC #PWR07
U 1 1 614840D3
P 2050 2400
F 0 "#PWR07" H 2050 2250 50  0001 C CNN
F 1 "VCC" H 2050 2550 50  0000 C CNN
F 2 "" H 2050 2400 50  0001 C CNN
F 3 "" H 2050 2400 50  0001 C CNN
	1    2050 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 2500 1500 2500
$Comp
L Device:R_Small R10
U 1 1 614840DA
P 1700 2700
F 0 "R10" V 1700 2700 39  0000 C CNN
F 1 "1K/1%" V 1600 2700 50  0000 C TNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1630 2700 50  0001 C CNN
F 3 "~" H 1700 2700 50  0001 C CNN
	1    1700 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 2800 1750 2800
Wire Wire Line
	1700 2600 1750 2600
$Comp
L power:GNDS #PWR02
U 1 1 614840E2
P 1400 2700
F 0 "#PWR02" H 1400 2450 50  0001 C CNN
F 1 "GNDS" H 1400 2550 50  0000 C CNN
F 2 "" H 1400 2700 50  0001 C CNN
F 3 "" H 1400 2700 50  0001 C CNN
	1    1400 2700
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C3
U 1 1 614840E8
P 1350 2500
F 0 "C3" V 1450 2500 50  0000 C CNN
F 1 "100pF" V 1500 2500 50  0000 C TNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1388 2350 50  0001 C CNN
F 3 "~" H 1350 2500 50  0001 C CNN
	1    1350 2500
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R3
U 1 1 614840EE
P 1500 2600
F 0 "R3" V 1500 2600 39  0000 C CNN
F 1 "1M/1%" H 1600 2800 50  0000 C TNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1430 2600 50  0001 C CNN
F 3 "~" H 1500 2600 50  0001 C CNN
	1    1500 2600
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C4
U 1 1 614840F4
P 1350 2900
F 0 "C4" V 1450 2900 50  0000 C CNN
F 1 "100pF" V 1500 2900 50  0000 C TNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1388 2750 50  0001 C CNN
F 3 "~" H 1350 2900 50  0001 C CNN
	1    1350 2900
	0    1    1    0   
$EndComp
Wire Wire Line
	1500 3950 1750 3950
Connection ~ 1500 3950
Connection ~ 1500 3750
Wire Wire Line
	1450 3950 1500 3950
$Comp
L Device:R_Small R6
U 1 1 6148F9D7
P 1500 3850
F 0 "R6" V 1500 3850 39  0000 C CNN
F 1 "1M/1%" H 1600 3700 50  0000 C TNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1430 3850 50  0001 C CNN
F 3 "~" H 1500 3850 50  0001 C CNN
	1    1500 3850
	1    0    0    -1  
$EndComp
Text Label 2500 3750 0    50   ~ 0
out3
Wire Wire Line
	2600 3750 2450 3750
Wire Wire Line
	1250 3950 1100 3950
Wire Wire Line
	1250 3550 1100 3550
Entry Wire Line
	2600 3750 2700 3850
Entry Wire Line
	1000 4050 1100 3950
Entry Wire Line
	1000 3650 1100 3550
$Comp
L power:GNDS #PWR015
U 1 1 6148F9E6
P 2150 4050
F 0 "#PWR015" H 2150 3800 50  0001 C CNN
F 1 "GNDS" V 2100 3850 50  0000 C CNN
F 2 "" H 2150 4050 50  0001 C CNN
F 3 "" H 2150 4050 50  0001 C CNN
	1    2150 4050
	0    -1   -1   0   
$EndComp
Connection ~ 2050 3450
Wire Wire Line
	2150 3450 2050 3450
Wire Wire Line
	1500 3550 1750 3550
Wire Wire Line
	1400 3750 1500 3750
Connection ~ 1500 3550
Wire Wire Line
	2350 3450 2400 3450
$Comp
L Device:C_Small C11
U 1 1 6148F9F2
P 2250 3450
F 0 "C11" V 2350 3450 50  0000 C CNN
F 1 "100nF" V 2100 3450 50  0000 C TNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2288 3300 50  0001 C CNN
F 3 "~" H 2250 3450 50  0001 C CNN
	1    2250 3450
	0    -1   -1   0   
$EndComp
$Comp
L power:Earth #PWR019
U 1 1 6148F9F8
P 2400 3450
F 0 "#PWR019" H 2400 3200 50  0001 C CNN
F 1 "Earth" H 2400 3300 50  0001 C CNN
F 2 "" H 2400 3450 50  0001 C CNN
F 3 "~" H 2400 3450 50  0001 C CNN
	1    2400 3450
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Instrumentation:INA128 U3
U 1 1 6148F9FE
P 2050 3750
F 0 "U3" H 2300 3650 50  0000 L CNN
F 1 "INA128" H 2350 3600 50  0000 C TNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 2150 3750 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/ina128.pdf" H 2150 3750 50  0001 C CNN
	1    2050 3750
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR010
U 1 1 6148FA04
P 2050 4050
F 0 "#PWR010" H 2050 3800 50  0001 C CNN
F 1 "Earth" H 2050 3900 50  0001 C CNN
F 2 "" H 2050 4050 50  0001 C CNN
F 3 "~" H 2050 4050 50  0001 C CNN
	1    2050 4050
	-1   0    0    -1  
$EndComp
$Comp
L power:VCC #PWR09
U 1 1 6148FA0A
P 2050 3450
F 0 "#PWR09" H 2050 3300 50  0001 C CNN
F 1 "VCC" H 2050 3600 50  0000 C CNN
F 2 "" H 2050 3450 50  0001 C CNN
F 3 "" H 2050 3450 50  0001 C CNN
	1    2050 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 3550 1500 3550
$Comp
L Device:R_Small R11
U 1 1 6148FA11
P 1700 3750
F 0 "R11" V 1700 3750 39  0000 C CNN
F 1 "1K/1%" V 1600 3750 50  0000 C TNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1630 3750 50  0001 C CNN
F 3 "~" H 1700 3750 50  0001 C CNN
	1    1700 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 3850 1750 3850
Wire Wire Line
	1700 3650 1750 3650
$Comp
L power:GNDS #PWR03
U 1 1 6148FA19
P 1400 3750
F 0 "#PWR03" H 1400 3500 50  0001 C CNN
F 1 "GNDS" H 1400 3600 50  0000 C CNN
F 2 "" H 1400 3750 50  0001 C CNN
F 3 "" H 1400 3750 50  0001 C CNN
	1    1400 3750
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C5
U 1 1 6148FA1F
P 1350 3550
F 0 "C5" V 1450 3550 50  0000 C CNN
F 1 "100pF" V 1500 3550 50  0000 C TNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1388 3400 50  0001 C CNN
F 3 "~" H 1350 3550 50  0001 C CNN
	1    1350 3550
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R5
U 1 1 6148FA25
P 1500 3650
F 0 "R5" V 1500 3650 39  0000 C CNN
F 1 "1M/1%" H 1600 3850 50  0000 C TNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1430 3650 50  0001 C CNN
F 3 "~" H 1500 3650 50  0001 C CNN
	1    1500 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C6
U 1 1 6148FA2B
P 1350 3950
F 0 "C6" V 1450 3950 50  0000 C CNN
F 1 "100pF" V 1500 3950 50  0000 C TNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1388 3800 50  0001 C CNN
F 3 "~" H 1350 3950 50  0001 C CNN
	1    1350 3950
	0    1    1    0   
$EndComp
Wire Wire Line
	1500 5000 1750 5000
Connection ~ 1500 5000
Connection ~ 1500 4800
Wire Wire Line
	1450 5000 1500 5000
$Comp
L Device:R_Small R8
U 1 1 61492F92
P 1500 4900
F 0 "R8" V 1500 4900 39  0000 C CNN
F 1 "1M/1%" H 1600 4750 50  0000 C TNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1430 4900 50  0001 C CNN
F 3 "~" H 1500 4900 50  0001 C CNN
	1    1500 4900
	1    0    0    -1  
$EndComp
Text Label 2500 4800 0    50   ~ 0
out4
Wire Wire Line
	2600 4800 2450 4800
Wire Wire Line
	1250 5000 1100 5000
Wire Wire Line
	1250 4600 1100 4600
Entry Wire Line
	2600 4800 2700 4900
Entry Wire Line
	1000 5100 1100 5000
Entry Wire Line
	1000 4700 1100 4600
$Comp
L power:GNDS #PWR016
U 1 1 61492FA1
P 2150 5100
F 0 "#PWR016" H 2150 4850 50  0001 C CNN
F 1 "GNDS" V 2100 4900 50  0000 C CNN
F 2 "" H 2150 5100 50  0001 C CNN
F 3 "" H 2150 5100 50  0001 C CNN
	1    2150 5100
	0    -1   -1   0   
$EndComp
Connection ~ 2050 4500
Wire Wire Line
	2150 4500 2050 4500
Wire Wire Line
	1500 4600 1750 4600
Wire Wire Line
	1400 4800 1500 4800
Connection ~ 1500 4600
Wire Wire Line
	2350 4500 2400 4500
$Comp
L Device:C_Small C12
U 1 1 61492FAD
P 2250 4500
F 0 "C12" V 2350 4500 50  0000 C CNN
F 1 "100nF" V 2100 4500 50  0000 C TNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2288 4350 50  0001 C CNN
F 3 "~" H 2250 4500 50  0001 C CNN
	1    2250 4500
	0    -1   -1   0   
$EndComp
$Comp
L power:Earth #PWR020
U 1 1 61492FB3
P 2400 4500
F 0 "#PWR020" H 2400 4250 50  0001 C CNN
F 1 "Earth" H 2400 4350 50  0001 C CNN
F 2 "" H 2400 4500 50  0001 C CNN
F 3 "~" H 2400 4500 50  0001 C CNN
	1    2400 4500
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Instrumentation:INA128 U4
U 1 1 61492FB9
P 2050 4800
F 0 "U4" H 2300 4700 50  0000 L CNN
F 1 "INA128" H 2350 4650 50  0000 C TNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 2150 4800 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/ina128.pdf" H 2150 4800 50  0001 C CNN
	1    2050 4800
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR012
U 1 1 61492FBF
P 2050 5100
F 0 "#PWR012" H 2050 4850 50  0001 C CNN
F 1 "Earth" H 2050 4950 50  0001 C CNN
F 2 "" H 2050 5100 50  0001 C CNN
F 3 "~" H 2050 5100 50  0001 C CNN
	1    2050 5100
	-1   0    0    -1  
$EndComp
$Comp
L power:VCC #PWR011
U 1 1 61492FC5
P 2050 4500
F 0 "#PWR011" H 2050 4350 50  0001 C CNN
F 1 "VCC" H 2050 4650 50  0000 C CNN
F 2 "" H 2050 4500 50  0001 C CNN
F 3 "" H 2050 4500 50  0001 C CNN
	1    2050 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 4600 1500 4600
$Comp
L Device:R_Small R12
U 1 1 61492FCC
P 1700 4800
F 0 "R12" V 1700 4800 39  0000 C CNN
F 1 "1K/1%" V 1600 4800 50  0000 C TNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1630 4800 50  0001 C CNN
F 3 "~" H 1700 4800 50  0001 C CNN
	1    1700 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 4900 1750 4900
Wire Wire Line
	1700 4700 1750 4700
$Comp
L power:GNDS #PWR04
U 1 1 61492FD4
P 1400 4800
F 0 "#PWR04" H 1400 4550 50  0001 C CNN
F 1 "GNDS" H 1400 4650 50  0000 C CNN
F 2 "" H 1400 4800 50  0001 C CNN
F 3 "" H 1400 4800 50  0001 C CNN
	1    1400 4800
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C7
U 1 1 61492FDA
P 1350 4600
F 0 "C7" V 1450 4600 50  0000 C CNN
F 1 "100pF" V 1500 4600 50  0000 C TNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1388 4450 50  0001 C CNN
F 3 "~" H 1350 4600 50  0001 C CNN
	1    1350 4600
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R7
U 1 1 61492FE0
P 1500 4700
F 0 "R7" V 1500 4700 39  0000 C CNN
F 1 "1M/1%" H 1600 4900 50  0000 C TNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1430 4700 50  0001 C CNN
F 3 "~" H 1500 4700 50  0001 C CNN
	1    1500 4700
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C8
U 1 1 61492FE6
P 1350 5000
F 0 "C8" V 1450 5000 50  0000 C CNN
F 1 "100pF" V 1500 5000 50  0000 C TNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1388 4850 50  0001 C CNN
F 3 "~" H 1350 5000 50  0001 C CNN
	1    1350 5000
	0    1    1    0   
$EndComp
Entry Wire Line
	2600 2700 2700 2800
Entry Wire Line
	2600 1650 2700 1750
Entry Bus Bus
	1000 5300 1100 5400
Entry Bus Bus
	2700 5300 2800 5400
Text Notes 3200 1100 0    50   ~ 0
Reference signal generator
Entry Bus Bus
	3150 5400 3250 5300
Entry Wire Line
	3250 5000 3350 4900
Entry Wire Line
	3250 5100 3350 5000
Entry Wire Line
	3250 5200 3350 5100
Entry Wire Line
	3250 5300 3350 5200
Wire Wire Line
	3900 4900 3350 4900
Wire Wire Line
	3350 5000 3900 5000
Wire Wire Line
	3900 5100 3350 5100
Wire Wire Line
	3350 5200 3900 5200
Text Label 3650 4900 0    50   ~ 0
in2
Text Label 3650 4450 0    50   ~ 0
in3
Text Label 3650 4550 0    50   ~ 0
in5
Text Label 3650 4650 0    50   ~ 0
in7
Wire Wire Line
	3350 4350 3900 4350
Wire Wire Line
	3350 4450 3900 4450
Wire Wire Line
	3350 4550 3900 4550
Wire Wire Line
	3350 4650 3900 4650
Entry Wire Line
	3250 4450 3350 4350
Entry Wire Line
	3250 4550 3350 4450
Entry Wire Line
	3250 4650 3350 4550
Entry Wire Line
	3250 4750 3350 4650
Text Label 3650 4350 0    50   ~ 0
in1
Text Label 3650 5000 0    50   ~ 0
in4
Text Label 3650 5100 0    50   ~ 0
in6
Text Label 3650 5200 0    50   ~ 0
in8
Wire Wire Line
	3350 3800 3900 3800
Wire Wire Line
	3350 3900 3900 3900
Wire Wire Line
	3350 4000 3900 4000
Wire Wire Line
	3350 4100 3900 4100
Entry Wire Line
	3250 3900 3350 3800
Entry Wire Line
	3250 4000 3350 3900
Entry Wire Line
	3250 4100 3350 4000
Entry Wire Line
	3250 4200 3350 4100
Text Label 3650 3800 0    50   ~ 0
out1
Text Label 3650 3900 0    50   ~ 0
out2
Text Label 3650 4000 0    50   ~ 0
out3
Text Label 3650 4100 0    50   ~ 0
out4
$Comp
L Connector_Generic:Conn_01x01 J2
U 1 1 6150CD44
P 4100 3450
F 0 "J2" H 4180 3492 50  0000 L CNN
F 1 "GroundElectrode" H 4180 3401 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 4100 3450 50  0001 C CNN
F 3 "~" H 4100 3450 50  0001 C CNN
	1    4100 3450
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 6150D40C
P 4100 3050
F 0 "J1" H 4180 3042 50  0000 L CNN
F 1 "PowerPlug" H 4180 2951 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4100 3050 50  0001 C CNN
F 3 "~" H 4100 3050 50  0001 C CNN
	1    4100 3050
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J3
U 1 1 6150DD4D
P 4100 3900
F 0 "J3" H 4180 3892 50  0000 L CNN
F 1 "OutputPins" H 4180 3801 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 4100 3900 50  0001 C CNN
F 3 "~" H 4100 3900 50  0001 C CNN
	1    4100 3900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J4
U 1 1 6150E64F
P 4100 4450
F 0 "J4" H 4180 4442 50  0000 L CNN
F 1 "InputPins1" H 4180 4351 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 4100 4450 50  0001 C CNN
F 3 "~" H 4100 4450 50  0001 C CNN
	1    4100 4450
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J5
U 1 1 6150F000
P 4100 5000
F 0 "J5" H 4180 4992 50  0000 L CNN
F 1 "InputPins2" H 4180 4901 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 4100 5000 50  0001 C CNN
F 3 "~" H 4100 5000 50  0001 C CNN
	1    4100 5000
	1    0    0    -1  
$EndComp
$Comp
L power:GNDS #PWR024
U 1 1 608FD740
P 3250 3450
F 0 "#PWR024" H 3250 3200 50  0001 C CNN
F 1 "GNDS" V 3250 3200 50  0000 C CNN
F 2 "" H 3250 3450 50  0001 C CNN
F 3 "" H 3250 3450 50  0001 C CNN
	1    3250 3450
	0    1    1    0   
$EndComp
$Comp
L Device:R R15
U 1 1 608772AE
P 3400 3450
F 0 "R15" V 3400 3450 50  0000 C CNN
F 1 "100k/1%" V 3500 3450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3330 3450 50  0001 C CNN
F 3 "~" H 3400 3450 50  0001 C CNN
	1    3400 3450
	0    1    1    0   
$EndComp
$Comp
L power:Earth #PWR028
U 1 1 6151A45F
P 3750 3150
F 0 "#PWR028" H 3750 2900 50  0001 C CNN
F 1 "Earth" H 3750 3000 50  0001 C CNN
F 2 "" H 3750 3150 50  0001 C CNN
F 3 "~" H 3750 3150 50  0001 C CNN
	1    3750 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 3050 3900 3050
Wire Wire Line
	3750 3150 3900 3150
Text Notes 1300 1100 0    50   ~ 0
Differential Amplifiers x4
Text Notes 3450 2900 0    50   ~ 0
Connectors
$Comp
L power:VCC #PWR027
U 1 1 6151AB70
P 3750 3050
F 0 "#PWR027" H 3750 2900 50  0001 C CNN
F 1 "VCC" V 3750 3200 50  0000 L CNN
F 2 "" H 3750 3050 50  0001 C CNN
F 3 "" H 3750 3050 50  0001 C CNN
	1    3750 3050
	0    -1   -1   0   
$EndComp
$Comp
L power:Earth #PWR029
U 1 1 61561441
P 4000 2100
F 0 "#PWR029" H 4000 1850 50  0001 C CNN
F 1 "Earth" H 4000 1950 50  0001 C CNN
F 2 "" H 4000 2100 50  0001 C CNN
F 3 "~" H 4000 2100 50  0001 C CNN
	1    4000 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 1800 3950 1800
$Comp
L Device:C C13
U 1 1 6155FF35
P 4000 1950
F 0 "C13" H 3900 1900 50  0000 R CNN
F 1 "100uF" H 3900 2000 50  0000 R CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4038 1800 50  0001 C CNN
F 3 "~" H 4000 1950 50  0001 C CNN
	1    4000 1950
	-1   0    0    1   
$EndComp
Wire Wire Line
	4450 1500 4450 2100
Wire Wire Line
	4150 1500 3550 1500
$Comp
L Amplifier_Operational:LM321 U5
U 1 1 615A420E
P 3650 1800
F 0 "U5" H 3650 1650 50  0000 L CNN
F 1 "LM321" H 3650 1550 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5_HandSoldering" H 3650 1800 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm321.pdf" H 3650 1800 50  0001 C CNN
	1    3650 1800
	1    0    0    -1  
$EndComp
Connection ~ 3550 1500
$Comp
L power:GNDS #PWR0101
U 1 1 6161A123
P 3350 2050
F 0 "#PWR0101" H 3350 1800 50  0001 C CNN
F 1 "GNDS" H 3350 1900 50  0000 C CNN
F 2 "" H 3350 2050 50  0001 C CNN
F 3 "" H 3350 2050 50  0001 C CNN
	1    3350 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 2050 3350 1900
Text Label 4000 1800 0    50   ~ 0
Vref
Wire Wire Line
	4000 1800 4150 1800
Connection ~ 4000 1800
Text Label 3200 1700 0    50   ~ 0
Vdiv
Text Label 3550 3450 0    50   ~ 0
GNDtrode
Wire Wire Line
	3550 3450 3900 3450
Wire Bus Line
	1100 5400 3150 5400
Wire Bus Line
	2700 1750 2700 5300
Wire Bus Line
	1000 1550 1000 5300
Wire Bus Line
	3250 3900 3250 5300
Text Label 1100 2900 0    50   ~ 0
in3
Text Label 1100 2500 0    50   ~ 0
in4
Text Label 1100 4600 0    50   ~ 0
in7
Text Label 1100 3950 0    50   ~ 0
in5
Text Label 1100 3550 0    50   ~ 0
in6
Text Label 1100 5000 0    50   ~ 0
in8
$EndSCHEMATC
